export const defaultAbilities = {
    Communication: 0,
    Constitution: 0,
	Cunning: 0,
    Dexterity: 0,
	Magic: 0,
    Perception: 0,
    Strength: 0,
    Willpower: 0,
};
